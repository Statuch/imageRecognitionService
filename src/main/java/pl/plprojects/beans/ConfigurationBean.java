package pl.plprojects.beans;

public class ConfigurationBean {
	public static final int visionMdbPort=100;
	public static final int visionAiPort=101;
	
	public static final int mdbWorkOnImageWidth = 200;
	public static final int mdbWorkOnImageHeight = 150;
	
	public static final int aiWorkOnImageWidth = 100;
	public static final int aiWorkOnImageHeight = 75;
	
	
	public static final String serverDir="C:/ImageRecServer/";
	public static final String workOnDir = "requests/";
	
	public static final String mdbAddress = "192.168.1.3";
	public static final String mdbUser = "system";
	public static final String mdbPassword = "mbdrafal";
	public static final String mdbSID = "mbd";
	public static final int mdbPort = 1028;
	public static final int mdbMaxPararelConnections = 3;
	
	public static final float imageProportion = 1.f;
	public static final float imageProportionOffset = 0.3f;
	public static final int imageMinSize = 12*12;
	public static final int aiImageWidth = 16;
	public static final int aiImageHeight = 16;
	
	public static final String imageRecognitionNetwork = "imageRecognitionNetwork.nnnet";
	
	public static final double minAISupport = 0.8f;
}
