package pl.plprojects.communicates;

public class RecognitionError {
	private final String error;
	private final String caused;
	
	public RecognitionError(String error, String caused) {
		this.error = error;
		this.caused = caused;
	}

	public String getError() {
		return error;
	}

	public String getCaused() {
		return caused;
	}
	
	
}
