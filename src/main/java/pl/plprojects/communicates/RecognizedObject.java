package pl.plprojects.communicates;

public class RecognizedObject {
	
	private final String object;
	private String state;
	private float scale;
	private final float support;
	private final String compare;
	
	public RecognizedObject(String object, String compare, float support) {
		this.object = object;
		this.compare = compare;
		this.support = support;
		
		//default values
		state = "N/A";
		scale = 1.0f;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}
	public String getObject() {
		return object;
	}
	public float getSupport() {
		return support;
	}
	public String getCompare() {
		return compare;
	}
}
