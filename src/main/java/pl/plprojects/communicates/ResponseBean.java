package pl.plprojects.communicates;

import java.util.ArrayList;
import java.util.List;

import org.omg.PortableInterceptor.SUCCESSFUL;

public class ResponseBean {
	public static final String STATE_SUCCESS = "SUCCESS";
	public static final String STATE_PASS = "PASS";
	public static final String STATE_ERROR = "ERROR";
	
	public static final String METHOD_MDB = "MDB";
	public static final String METHOD_AI = "AI";
	
	private String state;
	private final String method;
	private final List<RecognizedObject> founds;
	private final List<RecognitionError> errors;
	
	private String timeConsumed;
	private final int requestId;
	
	public ResponseBean(String method, int requestNo) {
		
		if(METHOD_AI.equals(method) || METHOD_MDB.equals(method)){
			this.method = method;
		}
		else throw new IllegalArgumentException("method must be MDB or AI");
		
		this.state = STATE_PASS;
		this.requestId = requestNo;
		
		founds = new ArrayList<RecognizedObject>();
		errors = new ArrayList<RecognitionError>();
		
		//defaults
		timeConsumed="inf";
		
		
	}

	public String getTimeConsumed() {
		return timeConsumed;
	}

	public String getMethod() {
		return method;
	}

	public void setTimeConsumed(String timeConsumed) {
		this.timeConsumed = timeConsumed;
	}

	public String getState() {
		if(founds.size() > 0){
			state = STATE_SUCCESS;
		}
		else if(errors.size()>0){
			state = STATE_ERROR;
		}
		else state = STATE_PASS;
		
		return state;
	}

	public List<RecognizedObject> getFounds() {
		return founds;
	}

	public List<RecognitionError> getErrors() {
		return errors;
	}

	public int getRequestId() {
		return requestId;
	}	
	
	
}
