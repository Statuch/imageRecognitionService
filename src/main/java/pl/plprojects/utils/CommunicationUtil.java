package pl.plprojects.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import pl.plprojects.communicates.RecognitionError;
import pl.plprojects.communicates.RecognizedObject;
import pl.plprojects.communicates.ResponseBean;

public class CommunicationUtil {
	public static JSONObject buildJsonResponse(ResponseBean response){
		JSONObject jsonResponse = new JSONObject();
		
		//main
		jsonResponse.put("state", response.getState());
		jsonResponse.put("request_id", response.getRequestId());
		jsonResponse.put("time_consume", response.getTimeConsumed());
		jsonResponse.put("method", response.getMethod());
		
		//if success build success table
		//if(ResponseBean.STATE_SUCCESS.equals(response.getState())){
		JSONArray success = new JSONArray();
		for(RecognizedObject found : response.getFounds()){
			JSONObject tmpSuccess = new JSONObject();
			tmpSuccess.put("object", found.getObject());
			tmpSuccess.put("state", found.getState());
			tmpSuccess.put("scale", found.getScale());
			tmpSuccess.put("support", found.getSupport());
			tmpSuccess.put("compare", found.getCompare());
			success.add(tmpSuccess);
		}
		jsonResponse.put("founds", success);
		//}
		
		//errors
		//if(ResponseBean.STATE_ERROR.equals(response.getState())){
		JSONArray errors = new JSONArray();
		for(RecognitionError error : response.getErrors()){
			JSONObject tmpError = new JSONObject();
			tmpError.put("error", error.getError());
			tmpError.put("cause", error.getCaused());
			errors.add(tmpError);
		}
		jsonResponse.put("errors", errors);
		//}
		//return
		return jsonResponse;
	}
}
