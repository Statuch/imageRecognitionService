package pl.plprojects.utils;

public class ExponentialSmoothing {

	/**
	 * Funkcja realizuje proste wygladznie wykladnicze
	 * @param series
	 * @param lambda
	 * @return
	 */
	public static double[] smooth(double[] series, double lambda){
		double[] responce = new double[series.length];
		
		for(int idx = 0, size=series.length; idx<size; idx++){
			
			responce[idx] = Math.pow(1-lambda, idx-1)*series[0];
			
			double tmpValue=0;
			
			for(int i=1; i<idx; i++){
				tmpValue+=Math.pow(1-lambda, i-1)*series[idx-i];
			}
			
			responce[idx]+=lambda*tmpValue;
		}
		
		return responce;
	}
}
