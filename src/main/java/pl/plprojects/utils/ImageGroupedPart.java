package pl.plprojects.utils;

import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

public class ImageGroupedPart {

	private ImageGroupedPart joined = null;

	private int startX;
	private int startY;
	private int endX;
	private int endY;
	private int primaryColor;
	private Set<Integer> colors;
	
	public ImageGroupedPart(int x, int y, int color) {
		this.startX = x;
		this.startY = y;
		this.endX = x;
		this.endY = y;
		primaryColor = color;
		colors = new TreeSet<Integer>();
		colors.add(primaryColor);
	}

	public ImageGroupedPart joinGroups(ImageGroupedPart group) {
		// laczyc grupy mozna tylko raz
		group = group.getMasterGroup();
		ImageGroupedPart master = this.getMasterGroup();
		
		if(master != group ){
			master.joined = group;
		}
		
		group.addPoint(master.startX, master.startY);
		group.addPoint(master.endX, master.endY);
		//add all colors
		for(int color : master.colors) group.addColor(color);
		
		return group;
	}

	public void addPoint(int x, int y) {
		if (joined == null) {
			this.startX = startX < x ? startX : x;
			this.startY = startY < y ? startY : y;
			this.endX = endX > x ? endX : x;
			this.endY = endY > y ? endY : y;
		} else {
			joined = getMasterGroup();
			joined.addPoint(x, y);
		}
	}

	public boolean isJoined() {
		return joined != null;
	}

	public ImageGroupedPart getJoined() {
		return joined;
	}

	public void setJoined(ImageGroupedPart joined) {
		this.joined = joined;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public int getEndX() {
		return endX;
	}

	public void setEndX(int endX) {
		this.endX = endX;
	}

	public int getEndY() {
		return endY;
	}

	public void setEndY(int endY) {
		this.endY = endY;
	}

	
	public int getSquere() {
		return (endX - startX) * (endY - startY);
	}
	
	public void addColor(int color){
		this.colors.add(color);
	}
	
	public Set<Integer> getColors() {
		return colors;
	}
	
	public ImageGroupedPart getMasterGroup() {
		if(joined != null ){ 
			joined = joined.getMasterGroup(); 
			return joined;
		}
		else return this;
	}
	
	public int getPrimaryColor() {
		return primaryColor;
	}
}
