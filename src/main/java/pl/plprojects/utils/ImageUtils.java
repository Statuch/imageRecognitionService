package pl.plprojects.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import pl.plprojects.beans.ConfigurationBean;
import pl.plprojects.visionServer.AiImageFilter;

public class ImageUtils {

	public static Histogram buildHistogram(BufferedImage image) {
		double[] red = new double[256];
		double[] green = new double[256];
		double[] blue = new double[256];
		double[] luminance = new double[256];

		for (int x = 0, weight = image.getWidth(); x < weight; x++) {
			for (int y = 0, height = image.getHeight(); y < height; y++) {
				Color color = new Color(image.getRGB(x, y));

				int redIdx = color.getRed();
				red[redIdx]++;

				int blueIdx = color.getBlue();
				blue[blueIdx]++;

				int greenIdx = color.getGreen();
				green[greenIdx]++;
				
				int luminanceIdx = (int)(greenIdx+redIdx+blueIdx)/3;
				luminance[luminanceIdx]++;
			}
		}
		Histogram response = new Histogram(red, blue, green, luminance);
		return response;
	}

	public static Histogram normalizeHistogram(int values, Histogram histogram) {

		double red[] = histogram.red;
		double blue[] = histogram.blue;
		double green[] = histogram.red;
		double luminance[] = histogram.luminance;
		
		//RED color
		double respRed[] = new double[values];
		double redTotal = 0;
		for (int idx = 0, size = red.length; idx < size; idx++) {
			int tmpIdx = idx / (size / values);
			respRed[tmpIdx] += red[idx];
			redTotal +=red[idx];
		}
		
		for (int idx = 0; idx < values; idx++) {
			respRed[idx]/=redTotal;
		}
		
		//BLUE color
		double respBlue[] = new double[values];
		double blueTotal = 0;
		for (int idx = 0, size = blue.length; idx < size; idx++) {
			int tmpIdx = idx / (size / values);
			respBlue[tmpIdx] += blue[idx];
			blueTotal+=blue[idx];
		}

		for (int idx = 0; idx < values; idx++) {
			respBlue[idx]/=blueTotal;
		}
		
		// GREEN total
		double respGreen[] = new double[values];
		double greenTotal = 0;
		for (int idx = 0, size = green.length; idx < size; idx++) {
			int tmpIdx = idx / (size / values);
			respGreen[tmpIdx] += green[idx];
			greenTotal+=green[idx];
		}
		
		for (int idx = 0; idx < values; idx++) {
			respGreen[idx]/=greenTotal;
		}
		
		// LUMINANCE total
		double respLuminance[] = new double[values];
		double luminanceTotal = 0;
		for (int idx = 0, size = luminance.length; idx < size; idx++) {
			int tmpIdx = idx / (size / values);
			respLuminance[tmpIdx] += luminance[idx];
			luminanceTotal+=luminance[idx];
		}
		
		for (int idx = 0; idx < values; idx++) {
			respGreen[idx]/=greenTotal;
		}
		
		return new Histogram(respRed, respBlue, respGreen, respLuminance);
	}

	public static final class Histogram {
		private final double[] red;
		private final double[] blue;
		private final double[] green;
		private final double[] luminance;
		
		private Histogram(double[] red, double[] blue, double[] green, double[] luminance) {
			this.blue = blue;
			this.green = green;
			this.red = red;
			this.luminance = luminance;
		}
		
		public double[] getLuminance() {
			return luminance;
		}
		
		public double[] getRed(){
			return red;
		}
		
		public double[] getBlue(){
			return blue;
		}
		
		public double[] getGreen(){
			return green;
		}
		
		
	}

	public static BufferedImage getThreshold(BufferedImage img,int min, int max) {
		int height = img.getHeight();
		int width = img.getWidth();
		BufferedImage finalThresholdImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB) ;
		
		int red = 0;
		int green = 0;
		int blue = 0;
		
		for (int x = 0; x < width; x++) {
			try {

				for (int y = 0; y < height; y++) {
					int color = img.getRGB(x, y);
					Color rgbColor = new Color(color);
					red = rgbColor.getRed();
					green = rgbColor.getGreen();
					blue = rgbColor.getBlue();

					if((red+green+green)/3 >= min && 
						(red+green+green)/3 <= max ) 
					{
						finalThresholdImage.setRGB(x,y,Color.WHITE.getRGB());
					}
						else {
							finalThresholdImage.setRGB(x,y,Color.BLACK.getRGB());
						}
					
				}
			} catch (Exception e) {
				 e.getMessage();
			}
		}
		
		return removeSinglePixelsFromTrashHold(finalThresholdImage);
	}
	
	static BufferedImage removeSinglePixelsFromTrashHold(BufferedImage image){
		for(int x =0, sizeX=image.getWidth()-1; x<sizeX; x++){
			for(int y =0, sizeY=image.getHeight()-1; y<sizeY; y++){
				
				//sprawdz czy w otoczeniu pliku jest choc jeden czarny piksel, jesli nie zamien go na bialy.
				int currentColor = image.getRGB(x, y);
				boolean toChange = true;
				
				int startX = x>0?x-1:0;
				int endX = x<sizeX?x+1:sizeX;
				
				int startY = y>0?y-1:0;
				int endY = y<sizeY?y+1:sizeY;
				
				for(int i = startX; i<=endX; i++){
					for(int j = startY; j<=endY; j++){
						if(x==i && y==j) continue;
						if(image.getRGB(i, j)==currentColor){
							toChange=false;
						}
						if(toChange==false) break;
					}
					if(toChange==false) break;
				}
				
				if(toChange==true)
					image.setRGB(endX, startY, currentColor^Color.WHITE.getRGB());		
			}
		}
		return image;
		
	}
	
	public static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
	 
		return resizedImage;
	    }
	
	public static List<BufferedImage> findAndCutObjects(BufferedImage originalImage, BufferedImage segmentMap){
		List<BufferedImage> response = new ArrayList<BufferedImage>();
		
		Map<Integer, ImageJSegObject> objects = new HashMap<Integer, ImageJSegObject>();
		//find segments:
		for(int j = 0, height = segmentMap.getHeight(); j<height; j++){
			for(int i = 0, width = segmentMap.getWidth(); i< width; i++){
			
				int tmpColor = segmentMap.getRGB(i, j);
				if(objects.containsKey(tmpColor)){
					objects.get(tmpColor).addPoint(i, j);
				}
				else{
					//ImageJSegObject tmp = new ImageUtils.ImageJSegObject(1, 2 , 1);
					objects.put(tmpColor, new ImageJSegObject(i, j , tmpColor) );
				}
			}	
		}
		
		//cut objects from oryginal image;
		for(Integer key: objects.keySet() ){
			ImageJSegObject tmpImgeSeg = objects.get(key);
			BufferedImage tmpImagePart = originalImage.getSubimage(tmpImgeSeg.startX, tmpImgeSeg.startY, tmpImgeSeg.getWidth(), tmpImgeSeg.getHeight());
			
			//pomijamy obrazy zbyt duze lub zbyt małe
			if(tmpImgeSeg.getHeight()*tmpImgeSeg.getWidth() < 20*20 || tmpImgeSeg.getHeight()*tmpImgeSeg.getWidth() > 100*100) continue;
			
			//tworzy tymczasowy wycinek mapy
			BufferedImage tmpMapPart = segmentMap.getSubimage(tmpImgeSeg.startX, tmpImgeSeg.startY, tmpImgeSeg.getWidth(), tmpImgeSeg.getHeight());
			BufferedImage objectImg = new BufferedImage(tmpImagePart.getWidth(), tmpImagePart.getHeight(), tmpImagePart.getType());
			
			//teraz przerysowywujemy i usuwamy elementy zbedne  z nowego obrazu poprzez zamienienie koloru na czarny;
			for(int j = 0, tmpImageHeight = tmpImgeSeg.getHeight(); j< tmpImageHeight; j++){
				for(int i = 0, tmpImageWidht= tmpImgeSeg.getWidth(); i< tmpImageWidht; i++){
					int currentSegmentColor = tmpMapPart.getRGB(i, j);
					if(currentSegmentColor != tmpImgeSeg.color){
						objectImg.setRGB(i, j, Color.black.getRGB());
					}
					else{
						objectImg.setRGB(i,j, tmpImagePart.getRGB(i, j));
					}
				}
			}
			//zapisujemy obiekt do odpowiedzi.
			response.add(objectImg);
		}
		return response;
	}
	static class ImageJSegObject{
		public ImageJSegObject(int startX, int startY, int color) {
			this.startX = startX;
			this.startY = startY;
			this.endX = startX;
			this.endY = startY;
			this.color = color;
		}
		int startX;
		int startY;
		int endX;
		int endY;
		
		int color;
		
		void addPoint(int x, int y){
			
			if(x<startX) startX = x;
			else if(x>endX) endX = x;
			
			if(y<startY) startY = y;
			else if (y>endY) endY = y;
		}
		
		int getWidth(){
			return endX-startX+1;
		}
		
		int getHeight(){
			return endY-startY+1;
		}
	}
	
	
	public static List<BufferedImage> findRegionsInTrashHold(BufferedImage trashHold, AiImageFilter filter ){
		int[][] map = new int [trashHold.getWidth()][trashHold.getHeight()];
		
		Map<Integer, ImageGroupedPart> parts = new HashMap<Integer, ImageGroupedPart>();
		List<BufferedImage> images = new ArrayList<BufferedImage>();
		for(int y = 0, height = trashHold.getHeight(); y<height; y++){
			for(int x =0, width = trashHold.getWidth(); x<width; x++){
				if(trashHold.getRGB(x, y)==Color.WHITE.getRGB()){
					int tmpColor = 0;
					if(y>0 && map[x][y-1]!=0){
						//gdy jest wiersz wyzej
						tmpColor = map[x][y-1];
					}
					
					if(x>0 && map[x-1][y]!=0){
						//gdy jest wiersz wczesnije
						//gdy kolor z gory jest rozny od koloru z lewej i nie są czarne
						int tmpLeftColor = map[x-1][y];
						if(tmpColor !=0 && tmpLeftColor!= tmpColor){
							tmpColor = parts.get(tmpColor).joinGroups(parts.get(tmpLeftColor)).getPrimaryColor();
						}
						else if(tmpColor == 0){
							tmpColor = tmpLeftColor;
						}
					}
					ImageGroupedPart newGroup =null;
					//jesli mimo wszystko nie mozna pobrac koloru z otoczenia to wez kolejny
					if(tmpColor == 0){
						tmpColor = parts.size()+1;
						newGroup = new ImageGroupedPart(x, y, tmpColor);
						parts.put(tmpColor, newGroup);
					}
					else{
						newGroup = parts.get(tmpColor);
					}
					newGroup.addPoint(x, y);
					//jesli mimo wszystko zaden z kolorow nie jest ustawiony to stwroz nowa grupe
					map[x][y]= tmpColor;
				}
				else{
					map[x][y] = 0;
				}
			}
		}
		
		for(ImageGroupedPart part: parts.values()){
			// if is not joind and pass the filter
			if(!part.isJoined() && filter.validate(part) ){
			
				//zamiast wycinania wez stworz nowe zdjęcie
				BufferedImage objectImage = new BufferedImage(
												part.getEndX()-part.getStartX()+1, 
												part.getEndY()-part.getStartY()+1, 
												trashHold.getType());
				
				BufferedImage tmpObjectTrashhold = trashHold.getSubimage(	part.getStartX(), 
																			part.getStartY(), 
																			part.getEndX()-part.getStartX()+1, 
																			part.getEndY()-part.getStartY()+1);
				
				for(int partY = 0, partHeight=tmpObjectTrashhold.getHeight(); partY>partHeight; partY++){
					for(int partX = 0, partWidth=tmpObjectTrashhold.getWidth(); partX>partWidth; partX++){
						
						int color = map[partX][partY];
						if(color!=0 && part.getColors().contains(color)){
							objectImage.setRGB(partX, partY, Color.white.getRGB());
						}
						else{
							objectImage.setRGB(partX, partY, Color.black.getRGB());
						}
					}
				}
				ImageUtils.storeTestImage(objectImage);
				//przeskalowanie zdjęcia do odpowiedniego rozmiaru
				BufferedImage imageToRecognize = ImageUtils.resizeImage(objectImage, objectImage.getType(), ConfigurationBean.aiImageWidth, ConfigurationBean.aiImageHeight);
				
				images.add(imageToRecognize);
			}
		}
		return images;
	}
	
	static int logImageNo = 0;
	public static void storeTestImage(BufferedImage image){
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			
			String location = "C:/ImageRecServer/imageLog/"+dateFormat.format(date)+"_"+logImageNo+".png";
			logImageNo++;
			File outputfile = new File(location);
		
			ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}