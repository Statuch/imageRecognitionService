package pl.plprojects.utils;

import java.io.File;
import java.io.IOException;

import pl.plprojects.beans.ConfigurationBean;


public class ManagementUtils {
	public static void cleanRequestsFolder()throws IOException{
		try{
			File requestFolder = new File( ConfigurationBean.serverDir + ConfigurationBean.workOnDir );
			for(File file : requestFolder.listFiles() ){
				if(file.isDirectory()){
					String fileName = file.getName();
					try{
						int requestId = Integer.parseInt(fileName);
						removeRequestFolder(requestId);
					}
					catch(NumberFormatException nfexc){
						// ignore this folder / file
					}
				}
			}
			
		}catch(IOException exc){
			throw new IOException("Error while cleaning server's request directory "+ exc);
		}
	}
	
	public static void removeRequestFolder(int requestId) throws IOException{
		try{
		File requestFolder = new File(
				ConfigurationBean.serverDir
				+ConfigurationBean.workOnDir
				+"/"+requestId);
		
		delete(requestFolder);
		}catch(IOException exc){
			throw new IOException("Exception while removing request form "+requestId, exc);
		}
	}
	
	private static void delete(File file) throws IOException{
	 
	    	if(file.isDirectory()){
	 
	    		//directory is empty, then delete it
	    		if(file.list().length==0){
	 
	    		   file.delete();
	    		   System.out.println("Directory is deleted : " 
	                                                 + file.getAbsolutePath());
	 
	    		}else{
	 
	    		   //list all the directory contents
	        	   String files[] = file.list();
	 
	        	   for (String temp : files) {
	        	      //construct the file structure
	        	      File fileDelete = new File(file, temp);
	 
	        	      //recursive delete
	        	     delete(fileDelete);
	        	   }
	 
	        	   //check the directory again, if empty then delete it
	        	   if(file.list().length==0){
	           	     file.delete();
	        	     System.out.println("Directory is deleted : " 
	                                                  + file.getAbsolutePath());
	        	   }
	    		}
	 
	    	}else{
	    		//if file, then delete it
	    		file.delete();
	    		System.out.println("File is deleted : " + file.getAbsolutePath());
	    	}
	    }
}
