package pl.plprojects.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.imageio.ImageIO;
import javax.security.auth.login.Configuration;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.driver.OracleConnection;
import oracle.jdbc.internal.OracleTypes;
import oracle.ord.im.OrdImage;
import oracle.ord.im.OrdImageSignature;
import oracle.ord.im.OrdMediaUtil;

import pl.plprojects.beans.ConfigurationBean;
import pl.plprojects.communicates.RecognizedObject;

public class MultimediaDatabaseUtils {
	private static final Semaphore sematphore = new Semaphore(ConfigurationBean.mdbMaxPararelConnections);
	
	public final static OracleConnection connect() throws Exception{
		String connectString;
		Class.forName("oracle.jdbc.driver.OracleDriver");
		//thin->oci8
		connectString="jdbc:oracle:thin:@"+ConfigurationBean.mdbAddress+":"+ConfigurationBean.mdbPort+":"+ConfigurationBean.mdbSID;
		OracleConnection conn = (OracleConnection)DriverManager.getConnection(connectString, ConfigurationBean.mdbUser, ConfigurationBean.mdbPassword);
		conn.setAutoCommit(false);
		return conn;
	}
	
	/**
	 * 
	 * @param image
	 * @return
	 * @throws Exception
	 */
	public final static RecognizedObject findSimilar(BufferedImage image) throws Exception{
		
		sematphore.acquire();
		
		RecognizedObject recObject = null;
		String tmpClass = null;
	
		OracleConnection conn=null;
		OracleResultSet rs = null;
		
		try{
			//zainicjalizuj połącznie z baza danych
			conn = connect();
			OrdMediaUtil.imCompatibilityInit(conn);
			
			//get objects from database
			int index;
			Statement s = conn.createStatement();
			
			//upload ne image
			OrdImageSignature currentSignature = addNewRequestImage(image, conn);
			
			//compare it with images that are in database
			rs = (OracleResultSet) s.executeQuery("select * from objects_to_recog for update");
			
			//create my temp file signature
			String tmpObjectClass=null;
			
			while(rs.next()){
				index = rs.getInt("ID");
				float minSupport = rs.getFloat("MIN_SUPPORT");
				OrdImageSignature imgSig = (OrdImageSignature) rs.getCustomDatum("SIGATURE", OrdImageSignature.getFactory());
				OrdImage imgObj = (OrdImage)rs.getCustomDatum("IMAGE", OrdImage.getFactory());
				imgSig.generateSignature(imgObj);
				
				String objClass = rs.getString("CLASS");
				String objCondition = rs.getString("CONDITION");
				String compare = rs.getString("COMPARE");
				float score = OrdImageSignature.evaluateScore(currentSignature, imgSig, compare);
				
				if(score<=minSupport){
					//if( tmpObjectClass == null) tmpObjectClass = objClass; 
					//dodaj nowy obiekt do wyniku
					if(tmpClass == null || objClass.equals(objClass)){
						if(recObject == null){
							recObject =new RecognizedObject(objClass,compare, score);
							recObject.setState( objCondition );
						}
						else{
							recObject.setState( recObject.getState()+":"+objCondition );
						}
					}
					else return null;
				}
			}
			return recObject;
		}
		catch(Exception exc){
			throw exc;
		}
		finally{
			//proba zamkniecnia polaczenia z baza
			try{ rs.close(); } catch(Exception exc){}
			try{ conn.commit(); } catch(Exception exc){}
			try{ conn.close();} catch(Exception exc){}
			
			//zmiejszczenie semaforu umozliwia innym watkom wejscie do sekcji krytycznej (tej metody)
			sematphore.release();
		}
		
		
	}
	
	private static OrdImageSignature addNewRequestImage(BufferedImage image, OracleConnection conn) throws Exception{
		
		OraclePreparedStatement insertStatement = null;
		
		OraclePreparedStatement selectRowStatement = null;
		OracleResultSet selectRowResultSet = null;
		
		OraclePreparedStatement uploadStatement = null;
		
		RowId rowId=null;
		OrdImage currentImage=null;
		OrdImageSignature currentSignature=null;
		
		ByteArrayOutputStream os=null;
		InputStream is=null;
		
		//STEP 1st
		//try to add new row for image
		try{
			String insertString = "insert into requests_rec(image, sigature) values (ordsys.ordimage.init(), ordsys.ordimagesignature.init()) returning rowid into ? ";
			insertStatement = (OraclePreparedStatement)conn.prepareStatement(insertString);
			
			insertStatement.registerReturnParameter(1, OracleTypes.ROWID);
			
			int count = insertStatement.executeUpdate();
			if(count>0){
				ResultSet res = insertStatement.getReturnResultSet();
				res.next();
				rowId = res.getRowId(1);
				res.close();
			}
		}catch(Exception exc){
			throw exc;
		}
		finally{
			if(insertStatement != null) insertStatement.close();
		}
		
		//STEP 2nd
		//teraz pobierz dodany wiersz
		try{
			String selectRowString = "select * from requests_rec where rowid = ? for update ";
			selectRowStatement = (OraclePreparedStatement)conn.prepareStatement(selectRowString);
			selectRowStatement.setRowId(1, rowId);
			
			selectRowResultSet = (OracleResultSet)selectRowStatement.executeQuery();
			while(selectRowResultSet.next()){
				currentImage = (OrdImage)selectRowResultSet.getORAData("IMAGE", OrdImage.getORADataFactory());
				currentSignature = (OrdImageSignature) selectRowResultSet.getCustomDatum("SIGATURE", OrdImageSignature.getFactory());
				break;
			}
		}
		catch(Exception exc){
			throw exc;
		}
		finally{
			selectRowResultSet.close();
			selectRowStatement.close();
		}
		
		//STEP 3
		//try to crate ordimage from stream
		//store it in database
		try{
			os = new ByteArrayOutputStream();
			ImageIO.write(image, "png", os);
			is = new ByteArrayInputStream(os.toByteArray());
			currentImage.loadDataFromInputStream(is);
			currentSignature.generateSignature(currentImage);
			//store tmp image as requst image
			
			String uploadString = "update requests_rec set image=?, sigature=? where rowid = ?";
			uploadStatement = (OraclePreparedStatement)conn.prepareStatement(uploadString);
			uploadStatement.setORAData(1, currentImage);
			uploadStatement.setORAData(2, currentSignature);
			uploadStatement.setRowId(3, rowId);
			
			uploadStatement.execute();
		}
		catch(Exception exc){
			throw exc;
		}
		finally{
			//try to close statement
			try{ uploadStatement.close(); } catch(Exception exc){}
			try{ is.close(); } catch(Exception exc){}
			try{ os.close(); } catch(Exception exc){}
		}
		
		return currentSignature;
	}
	
	
}
