package pl.plprojects.utils;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class RequestViewer extends JFrame{
	
	
	private JLabel imageComponent;
	private JPanel infoPanel;
	
	//information	
	private JLabel labelMethod;
	private JLabel textMethod;
	
	private JLabel labelState;
	private JLabel textState;
	
	private JLabel labelTime;
	private JLabel textTime;
	
	private Thread timer;
	private RequestViewer(){
		super();
		timer = new Thread(new Timer(), "Timer");
	}
	
	private void initialize(int requestId) {
		
		setTitle("Request no.: "+requestId);
		setSize(400, 250);
		setLayout(new BorderLayout());
		
		JLabel tmpimageComponent = new JLabel();
		imageComponent = tmpimageComponent;
		add(imageComponent, BorderLayout.CENTER);
		
		infoPanel = new JPanel();
		infoPanel.setBounds(61, 11, 81, 140);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));

		textTime = new JLabel();
		labelTime = new JLabel("Time: ");
		
		textMethod = new JLabel();
		labelMethod = new JLabel("Method: ");
		
		textState = new JLabel();
		labelState = new JLabel("State: ");
		
		infoPanel.add(labelMethod);
		infoPanel.add(textMethod);
		
		infoPanel.add(labelState);
		infoPanel.add(textState);
		
		infoPanel.add(labelTime);
		infoPanel.add(textTime);
		
		add(infoPanel, BorderLayout.EAST);
	}

	public static RequestViewer getInstance(int requestId) {
		RequestViewer result = new RequestViewer();
		result.initialize(requestId);
		return result;
	}
	
	public void showImage(BufferedImage image) {
		imageComponent.setIcon(new ImageIcon(image));
		this.repaint();
	}
	
	public void setProgressState(String state){
		textState.setText(state);
		repaint();
	}
	
	public void setMethod(String method){
		textMethod.setText(method);
		repaint();
	}
	
	public void setTimes(long miliseconds){
		long seconds = TimeUnit.MICROSECONDS.toSeconds(miliseconds);
		textTime.setText(String.valueOf(seconds)+" sec");
		repaint();
	}
	
	class Timer implements Runnable{
		long miliseconds;
		public void run() {
			while(!Thread.currentThread().isInterrupted()){
				try{
					Thread.sleep(200);
					miliseconds+=200;
					RequestViewer.this.setTimes(miliseconds);
				}
				catch(Exception exc){
					
				}
			}
			
		}
	}
	public void start() {
		show();
		timer.start();
	}
	
	public void close() {
		timer.interrupt();
		hide();
	}
}
