package pl.plprojects.utils;

import java.util.ArrayList;
import java.util.List;

public class TrashHoldUtils {
	
	/**
	 * Zwraca listę przedziałów widma podejrzanych o bycie powiązanymi z obiektami na obrazie
	 * UWAGA: działa w oparciu o histogram jasności
	 * @param luminanceHistogram
	 * @return
	 */
	public static List<TrashHoldingExtreme> getTrashHoldingExtrems(double luminanceHistogram[]){
		
		List<TrashHoldingExtreme> response = new ArrayList<TrashHoldingExtreme>();
		
		List<Integer> maximums  = new ArrayList<Integer>();
		List<Integer> minimum  = new ArrayList<Integer>();
		
		for(int idx = 1, size = luminanceHistogram.length; idx<size-1; idx++){
			//min
			if(
				luminanceHistogram[idx]<luminanceHistogram[idx+1] 
				&& luminanceHistogram[idx]<luminanceHistogram[idx-1] 
			){
				minimum.add(idx);
			}
			//max
			if(
					luminanceHistogram[idx]>luminanceHistogram[idx+1] 
					&& luminanceHistogram[idx]>luminanceHistogram[idx-1] 
			){
				maximums.add(idx);
			}
		}
		
		//zlacz bliskie minima i maksima
		for(int i = 1; i<maximums.size(); i++)
		{
			int prev = maximums.get(i-1);
			int curr = maximums.get(i);
			if(curr-prev < 15){
				maximums.remove(i-1);
				
				for(int j = 0; j<minimum.size(); j++){
					if(minimum.get(j) > curr-15 && minimum.get(j)<curr){ minimum.remove(j);
					break;
					}
				}
				i--;
			}
			
		}
		
		//dla kazdego maksimum znajdz pobliskie minima.
		for(int middle : maximums){
			int tmpMin = 0;
			int tmpMax = luminanceHistogram.length-1;
			
			for(int min : minimum ){
				if(min<middle){
					tmpMin=min;
				}
				if(min>middle){
					tmpMax = min;
					break;
				}
			}
			
			response.add(new TrashHoldingExtreme(tmpMin, tmpMax));
		}
			
		
		return response;
		
	}
	
	public static class TrashHoldingExtreme{
		
		public final int max;
		public final int min;
		
		private TrashHoldingExtreme(int min, int max){
			this.min = min;
			this.max = max;
		}
		
		public int getMax() {
			return max;
		}
		
		public int getMin() {
			return min;
		}
	}
	
}
