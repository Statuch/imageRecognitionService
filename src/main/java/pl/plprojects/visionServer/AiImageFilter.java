package pl.plprojects.visionServer;

import pl.plprojects.beans.ConfigurationBean;
import pl.plprojects.utils.ImageGroupedPart;

public class AiImageFilter {

		private final float imageProportion;
		private final float imageProportionOffset;
		private final int imageMinSize;
	
		public AiImageFilter(float imageProportion, float imageProportionOffset, int imageMinSize) {
			this.imageMinSize = imageMinSize;
			this.imageProportion = imageProportion;
			this.imageProportionOffset = imageProportionOffset;
		}
		
		public boolean validate(ImageGroupedPart imagePart) {
			int width = imagePart.getEndX()-imagePart.getStartX()+1;
			int height = imagePart.getEndY()-imagePart.getStartY()+1;
			
			float proportion = width/height;
			
			return ( 	proportion <= imageProportion+imageProportionOffset &&  
						proportion <= imageProportion+imageProportionOffset && 
						width * height >= imageMinSize);

		}
}
