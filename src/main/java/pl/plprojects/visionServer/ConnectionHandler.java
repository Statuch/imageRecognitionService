package pl.plprojects.visionServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import pl.plprojects.utils.RequestViewer;

public abstract class ConnectionHandler implements Runnable{
	
	protected Socket clientSocket;
	
	protected ConnectionHandler(Socket socket){
		clientSocket = socket;
	}
	
	protected PrintWriter out = null;
	protected BufferedReader in = null;
	
	public abstract void process();
	
	
	public void run()
	{
		try{
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream() ));
			process();
		}
		catch(Exception exc){
			//do nothing
		}
		finally{
			
			//close output
			try{ 
				out.close();
			}catch(Exception exc){}
			
			//close input
			try{ 
				in.close();
			}catch(Exception exc){}
			
			//close soccet
			try{ 
				clientSocket.close();
			}catch(Exception exc){}
			
		}
	}
	
	protected void closeConnection() throws IOException{
		clientSocket.close();
	}
	
	public boolean setClientSocket(Socket clientSocket) {
		if(clientSocket == null) {
			this.clientSocket = clientSocket; 
			return true;
		}
		else return false;
	}
}
