package pl.plprojects.visionServer;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.imgrec.ImageRecognitionPlugin;

import pl.plprojects.beans.ConfigurationBean;
import pl.plprojects.communicates.RecognitionError;
import pl.plprojects.communicates.RecognizedObject;
import pl.plprojects.communicates.ResponseBean;
import pl.plprojects.utils.CommunicationUtil;
import pl.plprojects.utils.ExponentialSmoothing;
import pl.plprojects.utils.ImageGroupedPart;
import pl.plprojects.utils.ImageUtils;
import pl.plprojects.utils.MultimediaDatabaseUtils;
import pl.plprojects.utils.TrashHoldUtils;
import pl.plprojects.utils.TrashHoldUtils.TrashHoldingExtreme;

public class ImageAIRecognitionConnectionHandler  extends ConnectionHandler {

	private int requestIndex = 0;
	private OutputStream output;
	private ObjectOutputStream writer;
	ResponseBean response;
	
	protected ImageAIRecognitionConnectionHandler(Socket socket) {
		super(socket);
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public void process() {
		response = new ResponseBean("AI", requestIndex);
		try{
			initialize();
			requestIndex++;			
			
			//step 1 copy request image and rescale it to created folder. Name it image.jpg
			BufferedImage requestImage = ImageIO.read(ImageIO.createImageInputStream(clientSocket.getInputStream()));
			requestImage = ImageUtils.resizeImage(requestImage, 
											requestImage.getType(), 
											ConfigurationBean.aiWorkOnImageWidth, 
											ConfigurationBean.aiWorkOnImageHeight);
			
			ImageUtils.Histogram imageHist = ImageUtils.buildHistogram(requestImage);
			double[] smoothLumHist = ExponentialSmoothing.smooth(imageHist.getLuminance(), 0.4f);
			
			List<TrashHoldingExtreme> trasholds = TrashHoldUtils.getTrashHoldingExtrems(smoothLumHist);
			
			for(TrashHoldingExtreme trashold : trasholds) {
				BufferedImage trashHoldLayer = ImageUtils.getThreshold(requestImage, trashold.getMin(), trashold.getMax());

				//store for test trasholdLayer
				ImageUtils.storeTestImage(trashHoldLayer);
				
				AiImageFilter filter = new AiImageFilter(
												ConfigurationBean.imageProportion, 
												ConfigurationBean.imageProportionOffset, 
												ConfigurationBean.imageMinSize);
				List<BufferedImage> imageParts = ImageUtils.findRegionsInTrashHold(trashHoldLayer, filter);
				for(BufferedImage workOnPart : imageParts){
					
				    	NeuralNetwork nnet = NeuralNetwork.load(ConfigurationBean.imageRecognitionNetwork); // load trained neural network saved with Neuroph Studio
				    	ImageRecognitionPlugin imageRecognition = (ImageRecognitionPlugin)nnet.getPlugin(ImageRecognitionPlugin.class); // get the image recognition plugin from neural network

						try {
					         // image recognition is done here (specify some existing image file)
					        HashMap<String, Double> output = imageRecognition.recognizeImage(workOnPart);
					        
					        //temporary type - służy do sprawdzenia czy wynik nie pasuje do wielu typoów
					        String tmpType = null;
					        double support = 0;
					        for(String type : output.keySet()){
					        	
					        	if(output.get(type) > ConfigurationBean.minAISupport){
					        		if(tmpType == null || tmpType.equalsIgnoreCase(type)){
					        			tmpType = type;
					        			//wybierz lepszy support
					        			support = support>output.get(type)? support:output.get(type);
					        		}
					        		else{
					        			// wprzeciwnym wypadu dany fragment został zaklasyfikowany do wielu kategorii
					        			type = null;
					        			break;
					        		}
					        	}
					        }
					        if(tmpType != null)
					        	response.getFounds().add(new RecognizedObject(tmpType.toUpperCase(), "AI", (float)support));
					        
					        
					    } catch(Exception ioe) {
					        ioe.printStackTrace();
					    }
				}
			}
			//write response
			writer.writeObject(CommunicationUtil.buildJsonResponse(response).toString());
		}	
		catch(Exception exc){
			try {
				response.getErrors().add(new RecognitionError(exc.getMessage(), exc.getStackTrace().toString()));
				writer.writeObject(CommunicationUtil.buildJsonResponse(response).toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			exc.printStackTrace();
		}
		finally{
			try { output.close(); } catch(Exception e){e.printStackTrace();}
			try { writer.close(); } catch(Exception e){e.printStackTrace();}
			try {closeConnection();} catch(Exception e){e.printStackTrace();}
		}
	}
	
	private void initialize() throws IOException {
		requestIndex = AiVisionServer.getNextRequestId();		
		output = clientSocket.getOutputStream();
		writer = new ObjectOutputStream(output);
	}

}
