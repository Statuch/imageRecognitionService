package pl.plprojects.visionServer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.text.Segment;

import pl.plprojects.beans.ConfigurationBean;
import pl.plprojects.communicates.RecognitionError;
import pl.plprojects.communicates.RecognizedObject;
import pl.plprojects.communicates.ResponseBean;
import pl.plprojects.utils.CommunicationUtil;
import pl.plprojects.utils.ImageUtils;
import pl.plprojects.utils.ManagementUtils;
import pl.plprojects.utils.MultimediaDatabaseUtils;
import pl.plprojects.utils.RequestViewer;

public class ImageMdbRecognitionConnectionHandler extends ConnectionHandler {
	private int requestIndex;
	private RequestViewer viewer;
	private OutputStream output;
	private ObjectOutputStream writer;
	ResponseBean response;
	
	public ImageMdbRecognitionConnectionHandler( Socket socket) {
		super(socket);
		try{
			ManagementUtils.cleanRequestsFolder();
		}
		catch(Exception exc ){
			System.out.println("ImageMdbRecognitionConnectionHandler initialization problem");
			exc.printStackTrace();
		}
	}
	
	@Override
	public void process() {
		try{
			initialize();
			
			viewer = RequestViewer.getInstance(requestIndex);
			
			response = new ResponseBean("MDB", requestIndex);
			
			viewer.start();
			viewer.setMethod("Multimedia Database");
			viewer.setProgressState("Preparing data...");
			
			//step1:
			//crete temporery folder named #requestNo
			File workOnFolder = new File(ConfigurationBean.serverDir+ConfigurationBean.workOnDir+requestIndex);
			workOnFolder.mkdir();

			//step 2 copy request image and rescale it to created folder. Name it image.jpg
			BufferedImage image = ImageIO.read(ImageIO.createImageInputStream(clientSocket.getInputStream()));
			
			//show image
			viewer.showImage(image);
			viewer.setProgressState("Image received...");
			
			BufferedImage normalizedImage = ImageUtils.resizeImage(image, image.getType(), ConfigurationBean.mdbWorkOnImageWidth, ConfigurationBean.mdbWorkOnImageHeight);
			File imageFile = new File(workOnFolder.getPath()+"/image.jpg");
			ImageIO.write(
					normalizedImage
					, "jpg"
					, imageFile);
			
			//step 3 : use JSEG algorithm to separete objects
			Process process = Runtime.getRuntime().exec(
				"cmd /c start/wait "+ConfigurationBean.serverDir+"tools/segment.bat "+requestIndex
			);

			
			
			process.waitFor();
			
			viewer.setProgressState("Image segmentation...");
			
			//step 4 find objects from segment's map.
			File segmentsMap[] = workOnFolder.listFiles(new FilenameFilter() {
				
				public boolean accept(File file, String filename) {
					if ("segments.gif".equalsIgnoreCase(filename)) return true;
					return false;
				}
			});
			
			List<BufferedImage> foundObjects = new ArrayList<BufferedImage>();
			
			if(segmentsMap.length>0){
				for(File map: segmentsMap){
					foundObjects = ImageUtils.findAndCutObjects(normalizedImage, ImageIO.read(map));
					int imageIdx = 0;
					
					for(BufferedImage tmpImagePart : foundObjects){
						imageIdx++;
						ImageIO.write(tmpImagePart, "PNG", new File(workOnFolder.getPath()+"/"+imageIdx+".png"));	
												
						try{
							RecognizedObject obj = MultimediaDatabaseUtils.findSimilar(tmpImagePart);
							if(obj != null){
								response.getFounds().add(obj);
							}
						}
						catch(Exception exc){
							response.getErrors().add(new RecognitionError(exc.getMessage(), exc.getCause().getMessage()));
						}
					}
				}
			}
			
			//write response
			writer.writeObject(CommunicationUtil.buildJsonResponse(response).toString());
			
		}
		catch (IOException e) {
			out.print("Error while storing file");
		}
		catch(Exception exc){
			exc.printStackTrace();
		}
		finally{
			try { viewer.close(); } catch(Exception e){e.printStackTrace();}
			try { output.close(); } catch(Exception e){e.printStackTrace();}
			try { writer.close(); } catch(Exception e){e.printStackTrace();}
			try {closeConnection();} catch(Exception e){e.printStackTrace();}
		}
	}
	
	private void initialize() throws IOException {
		requestIndex = MdbVisionServer.getNextRequestId();		
		output = clientSocket.getOutputStream();
		writer = new ObjectOutputStream(output);
	}
	
	
}
