package pl.plprojects.visionServer;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import javax.imageio.ImageIO;

public class ImageStoreConnectionHandler extends ConnectionHandler {

	static int imageIdx= 0;
	
	public ImageStoreConnectionHandler( Socket socket) {
		super(socket);
	}
	
	@Override
	public void process() {
		try{
			

			BufferedImage image = ImageIO.read(ImageIO.createImageInputStream(clientSocket.getInputStream()));
			ImageIO.write(image, "PNG", new File("c:/images/"+imageIdx+".png"));
			imageIdx++;		
			//taka prowizorka do poprawy
			clientSocket.getOutputStream().write(new byte[]{});
		}
		catch (IOException e) {
			out.print("Error while storing file");
		}
	}

}
