package pl.plprojects.visionServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;

import pl.plprojects.utils.ManagementUtils;

public class MdbVisionServer extends ServerSocket{
	private static volatile int requestNo=0;
	
	//zapewnia mi ze zaden inny watek który bedzie z tego korzystal nie wezmie tego samego id
	public static synchronized int getNextRequestId(){
		return ++requestNo;
	}
	
	private Set<Socket> clients = null;
	
	public MdbVisionServer(int port) throws IOException {
		super(port);
	}

	public void start() throws IOException{
		ManagementUtils.cleanRequestsFolder();
		try{
			
			while(true){
				Socket clientSocket = accept();
				//Thread connectionHandler = new Thread(new ImageStoreConnectionHandler(clientSocket));
				Thread connectionHandler = new Thread(new ImageMdbRecognitionConnectionHandler(clientSocket), "RequestThread" );
				connectionHandler.start();
			}
		}
		catch (Exception e) {
			
		}		
		
		close();
	}
}
