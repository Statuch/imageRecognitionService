package pl.plprojects.visionServer;

import pl.plprojects.beans.ConfigurationBean;

public class ServerRun {
	public static void main(String[] args) {
		
		
			new Thread(new Runnable() {
				
				public void run() {
					
					try{
						MdbVisionServer mdbVisionServer = new MdbVisionServer(ConfigurationBean.visionMdbPort);
						mdbVisionServer.start();
					}
					catch(Exception exc){
						System.out.println(exc.getMessage());
					}
				}
			}, "MdbVisionServerThread").start();
			
			new Thread(new Runnable() {
				
				public void run() {
					
					try{
						AiVisionServer aiVisionServer = new AiVisionServer(ConfigurationBean.visionAiPort);
						aiVisionServer.start();
					}
					catch(Exception exc){
						System.out.println(exc.getMessage());
					}
				}
			}, "AiVisionServerThread").start();
			
		
	}
}
