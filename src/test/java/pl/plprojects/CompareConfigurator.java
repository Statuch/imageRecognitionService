package pl.plprojects;

import java.sql.Statement;

import javax.naming.spi.DirStateFactory.Result;

import oracle.jdbc.OracleResultSet;
import oracle.jdbc.driver.OracleConnection;
import oracle.ord.im.OrdImage;
import oracle.ord.im.OrdImageSignature;
import oracle.ord.im.OrdMediaUtil;
import pl.plprojects.utils.MultimediaDatabaseUtils;

public class CompareConfigurator {
	
	static OracleConnection conn;
	public static void main(String[] args) {
		
		try {
			//step 0 connect to database
			conn = MultimediaDatabaseUtils.connect();
			OrdMediaUtil.imCompatibilityInit(conn);

			//step 1 get all images from database;
			
			Statement s = conn.createStatement();
			
			//compare it with images that are in database
			OracleResultSet rs = (OracleResultSet) s.executeQuery("select * from objects_to_recog where id = 1 for update");
			
			//create my temp file signature
			String tmpObjectClass=null;
			
			while(rs.next()){
				OrdImageSignature imgSig = (OrdImageSignature) rs.getCustomDatum("SIGATURE", OrdImageSignature.getFactory());
				OrdImage imgObj = (OrdImage)rs.getCustomDatum("IMAGE", OrdImage.getFactory());
				imgSig.generateSignature(imgObj);
				
				String objClass = rs.getString("CLASS");
				String objCondition = rs.getString("CONDITION");
				float color =0.f;
				float shape = 0.f;
				float location = 0.f;
				
				float totalError = 0;
				int mistakes = 0;
				//tutaj dla kazdego obrazka trzeba probowac wszystkich mozliwych ustawien koloru czegos tam i czegos tam.
				String[] configurations = new String[]{
						"color=\"1,0\",shape=\"0,0\",location=\"0,0\"",
						"color=\"0,9\",shape=\"0,1\",location=\"0,0\"",
						"color=\"0,9\",shape=\"0,0\",location=\"0,1\"",
						"color=\"0,8\",shape=\"0,2\",location=\"0,0\"",
						"color=\"0,8\",shape=\"0,1\",location=\"0,1\"",
						"color=\"0,8\",shape=\"0,0\",location=\"0,2\"",
						"color=\"0,7\",shape=\"0,3\",location=\"0,0\"",
						"color=\"0,7\",shape=\"0,2\",location=\"0,1\"",
						"color=\"0,7\",shape=\"0,1\",location=\"0,2\"",
						"color=\"0,7\",shape=\"0,2\",location=\"0,3\"",
						"color=\"0,6\",shape=\"0,4\",location=\"0,0\"",
						"color=\"0,6\",shape=\"0,3\",location=\"0,1\"",
						"color=\"0,6\",shape=\"0,2\",location=\"0,2\"",
						"color=\"0,6\",shape=\"0,1\",location=\"0,3\"",
						"color=\"0,6\",shape=\"0,0\",location=\"0,4\"",
						"color=\"0,5\",shape=\"0,5\",location=\"0,0\"",
						"color=\"0,5\",shape=\"0,4\",location=\"0,1\"",
						"color=\"0,5\",shape=\"0,3\",location=\"0,2\"",
						"color=\"0,5\",shape=\"0,2\",location=\"0,3\"",
						"color=\"0,5\",shape=\"0,1\",location=\"0,4\"",
						"color=\"0,5\",shape=\"0,0\",location=\"0,5\"",
						"color=\"0,4\",shape=\"0,6\",location=\"0,0\"",
						"color=\"0,4\",shape=\"0,5\",location=\"0,1\"",
						"color=\"0,4\",shape=\"0,4\",location=\"0,2\"",
						"color=\"0,4\",shape=\"0,3\",location=\"0,3\"",
						"color=\"0,4\",shape=\"0,2\",location=\"0,4\"",
						"color=\"0,4\",shape=\"0,1\",location=\"0,5\"",
						"color=\"0,4\",shape=\"0,0\",location=\"0,6\"",
						"color=\"0,3\",shape=\"0,7\",location=\"0,0\"",
						"color=\"0,3\",shape=\"0,6\",location=\"0,1\"",
						"color=\"0,3\",shape=\"0,5\",location=\"0,2\"",
						"color=\"0,3\",shape=\"0,4\",location=\"0,3\"",
						"color=\"0,3\",shape=\"0,3\",location=\"0,4\"",
						"color=\"0,3\",shape=\"0,2\",location=\"0,5\"",
						"color=\"0,3\",shape=\"0,1\",location=\"0,6\"",
						"color=\"0,3\",shape=\"0,0\",location=\"0,7\"",
						"color=\"0,2\",shape=\"0,8\",location=\"0,0\"",
						"color=\"0,2\",shape=\"0,7\",location=\"0,1\"",
						"color=\"0,2\",shape=\"0,6\",location=\"0,2\"",
						"color=\"0,2\",shape=\"0,5\",location=\"0,3\"",
						"color=\"0,2\",shape=\"0,4\",location=\"0,4\"",
						"color=\"0,2\",shape=\"0,3\",location=\"0,5\"",
						"color=\"0,2\",shape=\"0,2\",location=\"0,6\"",
						"color=\"0,2\",shape=\"0,1\",location=\"0,7\"",
						"color=\"0,2\",shape=\"0,0\",location=\"0,8\"",
						"color=\"0,1\",shape=\"0,9\",location=\"0,0\"",
						"color=\"0,1\",shape=\"0,8\",location=\"0,1\"",
						"color=\"0,1\",shape=\"0,7\",location=\"0,2\"",
						"color=\"0,1\",shape=\"0,6\",location=\"0,3\"",
						"color=\"0,1\",shape=\"0,5\",location=\"0,4\"",
						"color=\"0,1\",shape=\"0,4\",location=\"0,5\"",
						"color=\"0,1\",shape=\"0,3\",location=\"0,6\"",
						"color=\"0,1\",shape=\"0,2\",location=\"0,7\"",
						"color=\"0,1\",shape=\"0,1\",location=\"0,8\"",
						"color=\"0,1\",shape=\"0,0\",location=\"0,9\"",
						"color=\"0,0\",shape=\"1,0\",location=\"0,0\"",
						"color=\"0,0\",shape=\"0,9\",location=\"0,1\"",
						"color=\"0,0\",shape=\"0,8\",location=\"0,2\"",
						"color=\"0,0\",shape=\"0,7\",location=\"0,3\"",
						"color=\"0,0\",shape=\"0,6\",location=\"0,4\"",
						"color=\"0,0\",shape=\"0,5\",location=\"0,5\"",
						"color=\"0,0\",shape=\"0,4\",location=\"0,6\"",
						"color=\"0,0\",shape=\"0,3\",location=\"0,7\"",
						"color=\"0,0\",shape=\"0,2\",location=\"0,8\"",
						"color=\"0,0\",shape=\"0,1\",location=\"0,9\"",
						"color=\"0,0\",shape=\"0,0\",location=\"1,0\""}; 
				for(String configuration : configurations){
						
						totalError = 0;
						mistakes = 0;
						
						//wez wszyskie zdjęcia z test set.
						Statement tmp = conn.createStatement();
						OracleResultSet tmpres = (OracleResultSet) s.executeQuery( "select * from objects_test_set for update" );
						
						while(tmpres.next()){
							String tmpClass = tmpres.getString("CLASS");
							OrdImageSignature tmpImgSig = (OrdImageSignature) tmpres.getCustomDatum("SIGATURE", OrdImageSignature.getFactory());
							OrdImage tmpImgObj = (OrdImage)tmpres.getCustomDatum("IMAGE", OrdImage.getFactory());
							tmpImgSig.generateSignature(tmpImgObj);
							
							float score = OrdImageSignature.evaluateScore(tmpImgSig, imgSig, configuration);
							
							//total error
							if(objClass.equalsIgnoreCase(tmpClass)){
								totalError+=score;
							}
							else{
								totalError+=100-score;
							}
							
							//mistakes
							if((	objClass.equalsIgnoreCase(tmpClass) && score>50) 
									|| (!objClass.equalsIgnoreCase(tmpClass) && score<50) ){
								mistakes++;
							}
						}
						try{
							tmp.close();	
						}
						catch(Exception exc){}
						try{
							tmpres.close();
						}
						catch(Exception exc){}
						
						
						System.out.println(objClass + "." +objCondition+" -> " + "["+configuration+"] totalError = "+totalError+", mistakes = "+mistakes);
				}
			}
			s.close();
			rs.close();
			conn.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
