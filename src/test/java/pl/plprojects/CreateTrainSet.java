package pl.plprojects;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import pl.plprojects.utils.ImageUtils;

public class CreateTrainSet {

	public static void main(String[] args) {
		String folder="C:/ImageRecServer/aiLearningSet/";
		String[] files= new String[]{"ball.png","jar.png","cup_right.png","cup_center.png","cup_left.png"};
		int imageWidth = 25;
		int imageHeight = 25;
		
		File tmpFolder = new File(folder+imageWidth+"_"+imageHeight);
		tmpFolder.mkdir();
		
		for(String file: files){
			
			
			BufferedImage image=null;
			try {
				image = ImageIO.read(new File(folder+file));
				image = ImageUtils.resizeImage(image , image.getType(), imageWidth, imageHeight);
				File tmpFile = new File(folder+imageWidth+"_"+imageHeight+"/"+file);
				tmpFile.createNewFile();
				ImageIO.write(image, "PNG", tmpFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int y=0, height=image.getHeight(); y<height ; y++){
				for(int x=0, width=image.getWidth(); x<width; x++){
					Color color = new Color(image.getRGB(x, y));
					if(color.getBlue()<128){
						System.out.print(1+",");
					}
					else{
						System.out.print(0+",");
					}
				}
				
			}
			System.out.print("1,0,0,0,0");
			System.out.println();
			
		}

	}

}
