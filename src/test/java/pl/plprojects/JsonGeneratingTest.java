package pl.plprojects;

import org.json.simple.JSONObject;
import org.omg.CORBA.Request;

import pl.plprojects.communicates.RecognitionError;
import pl.plprojects.communicates.RecognizedObject;
import pl.plprojects.communicates.ResponseBean;
import pl.plprojects.utils.CommunicationUtil;

import junit.framework.TestCase;

public class JsonGeneratingTest extends TestCase {
	ResponseBean success;
	ResponseBean pass;
	ResponseBean error;
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		success = new ResponseBean(	ResponseBean.METHOD_MDB, 1);
		success.setTimeConsumed("12 sec");
		success.getErrors().add(new RecognitionError("Null Pointer exception", "You idiot"));
		success.getErrors().add(new RecognitionError("Dzielenie przez zero", "You idiot"));
		
		success.getFounds().add(new RecognizedObject("cup", "color=1.0", 1));
		RecognizedObject obj = new RecognizedObject("ball", "color=1.0", 0.8f);
		obj.setScale(1.0f);
		obj.setState("right");
		success.getFounds().add(obj);		
	}
	
	public void testBasic() throws Exception {
		JSONObject obj = CommunicationUtil.buildJsonResponse(success);
		System.out.println(obj.toString());
	}
}
