package pl.plprojects;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.imageio.ImageIO;

public class VisionClient{
	
	public static void main(String[] args) {
		
		try {
			
			for(int i = 26; i<=40; i++){
				Socket socket = null;
				BufferedReader reader =null;
				try{
					
					socket = new Socket("localhost", 101);
					String imageFolder = "C:/ImageRecServer/samples/";
					
					BufferedImage tmpImage = ImageIO.read( new File(imageFolder+i+".jpg"));	
					ImageIO.write(tmpImage,"PNG",socket.getOutputStream());
			        
					ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
					
					try{
					String line=line = (String)inputStream.readObject();
					System.out.println(line);
					}catch(Exception exc){
						System.out.println("Exception cause that no response.");
					}
					
					
				}catch(Exception exc){
					exc.printStackTrace();
				}
				finally{
					try { socket.close(); } catch(Exception e){}
					try { reader.close(); } catch(Exception e){}
				}
			}
			   
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
	}
}
