package pl.plprojects;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.imageio.ImageIO;

public class imageModifiation{
	
	public static void main(String[] args) {
		
		try {
						
			BufferedImage tmpImage = ImageIO.read( new File("C:/ImageRecServer/tests/test.jpg"));	
			
			BufferedImage prtImage = tmpImage.getSubimage(200, 200,200 ,150 );
			for(int y = 0, height=prtImage.getHeight(); y<height; y++){	
				for(int x = 0, width=prtImage.getWidth(); x<width; x++){
					prtImage.setRGB(x, y, Color.red.getRGB());
				}
			}
			ImageIO.write(prtImage, "PNG", new File("C:/ImageRecServer/tests/result_prt.png"));
			ImageIO.write(tmpImage, "PNG", new File("C:/ImageRecServer/tests/result.png"));
		}catch(Exception exc){
			exc.printStackTrace();
		}
		
	}
}
